<!DOCTYPE html>
<html>
<head>
    <title>Project</title>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link rel="stylesheet" media="screen" href="{{ URL::asset('css/bootstrap.min.css') }}">

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="product_container" class="row">
        <div class="col-xs-3 center_content">
            <div class="col-xs-12 form-group">
                <label for="product_name" class="control-label">Product Name:</label>
                <div class="controls">
                    <input type="text" class="product_name form-control">
                </div>
            </div>
            <div class="col-xs-12 form-group account_last_name_container">
                <label for="qty_in_stock" class="control-label">Quantity In Stock:</label>
                <div class="controls">
                    <input type="text" class="qty_in_stock form-control">
                </div>
            </div>
            <div class="col-xs-12 form-group account_email_container">
                <label for="price" class="control-label">Price:</label>
                <div class="controls">
                    <input type="text" class="price form-control">
                </div>
            </div>
            <div class="col-xs-12">
                <button type="button" class="btn btn-sm btn-success pull-right save_button">Save</button>
            </div>
        </div>
    </div>
    <div class="row result">

    </div>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-2.2.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/backbone-min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/project.js') }}"></script>

</body>
</html>