<?php
echo '<?xml version="1.0" encoding="UTF-8"?>'
?>
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
    </Header>
    <Product>{{ $data->input('product_name') }}</Product>
    <Qty>{{ $data->input('qty_in_stock') }}</Qty>
    <Price>{{ $data->input('price') }}</Price>
    <Total>{{ $data->input('qty_in_stock') * $data->input('price') }}</Total>
