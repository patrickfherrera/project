/**
 * Created by Patrick on 5/17/2017.
 */
ProjectFunctions = Backbone.View.extend({
    el: '#product_container',

    events: {
        'click .save_button': 'save'
    },

    save: function(e) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'products/save',
            type: 'POST',
            data: {
                'product_name' : $('.product_name').val(),
                'qty_in_stock' : $('.qty_in_stock').val(),
                'price'        : $('.price').val()
            },
            success: function(data) {
                console.log(data);
                $('.result').text(data);
            }
        })
    }

})

new ProjectFunctions();