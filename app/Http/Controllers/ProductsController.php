<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ProductsController extends Controller {

    public function index()
    {
        return View::make('products');
    }

    public function save(Request $request)
    {
        $xml = View::make('xml', [
            'data'  => $request,
        ])->render();

        return response()->json($xml);
    }

}